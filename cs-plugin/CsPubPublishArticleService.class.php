<?php
/**
 * PublishArticle Publishing service.
 *
 * @package Enterprise
 * @subpackage Services
 * @since v10.2
 * @copyright WoodWing Software bv. All Rights Reserved.
 */

require_once __DIR__.'/../../interfaces/services/pub/CsPubPublishArticleRequest.class.php';
require_once __DIR__.'/../../interfaces/services/pub/CsPubPublishArticleResponse.class.php';
require_once BASEDIR.'/server/services/EnterpriseService.class.php';

class CsPubPublishArticleService extends EnterpriseService
{
	public function execute( CsPubPublishArticleRequest $req )
	{
		return $this->executeService( 
			$req, 
			$req->Ticket, 
			'ContentStationPublishingService',
			'CsPubPublishArticle',
			true,  		// check ticket
			true   	// use transactions
			);
	}

	public function runCallback( CsPubPublishArticleRequest $req )
	{
		if( !$req->Action ) {
			$message = 'Publish action requires Action parameter which is not provided.';
			throw new BizException( 'ERR_ARGUMENT', 'Client', $message );
		}
		if( !$req->ArticleId ) {
			$message = 'Publish action requires ArticleId parameter which is not provided.';
			throw new BizException( 'ERR_ARGUMENT', 'Client', $message );
		}
		if( $req->ArticleFiles ) foreach( $req->ArticleFiles as $file ) {
			if( $file->File->Rendition == 'native' ) {
				$message = 'The ArticleFiles parameter of the Publish action should not contain an Attachment for which '.
					'the Rendition is set to "native".';
				throw new BizException( 'ERR_ARGUMENT', 'Client', $message );
			}
		}

		// GloveLike-Mod start: 
		// Add hook to publish process so we can keep track of some things
		if ( file_exists(BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php') ){
			require_once (BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php');
			if ( function_exists( 'DigitalPublishHook' ) ){
				DigitalPublishHook( $req );
			}
		}
		// GloveLike-Mod End: 


		$properties = $req->Properties ? json_decode( $req->Properties ) : null;

		require_once __DIR__.'/../../bizclasses/Publisher.class.php';
		$publisher = new ContentStation_BizClasses_Publisher();
		$publisher->publishArticle( $req->Action, $req->ArticleId, $req->PubChannelId,
			$properties, $req->ArticleFiles );
		return new CsPubPublishArticleResponse();
	}
}

        // GloveLike-Mod start: 
		// Add hook to publish process so we can keep track of some things
		if ( file_exists(BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php') ){
			require_once (BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php');
			if ( function_exists( 'DigitalPublishHook' ) ){
				DigitalPublishHook( $req );
			}
		}
		// GloveLike-Mod End: 
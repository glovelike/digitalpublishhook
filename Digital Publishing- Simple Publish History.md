## Digital Publishing: Simple Publish History

In the standard WoodWing Aurora system, there is no history or overview to where digital articles are published and by who.

This Enterprise Plugin will record publish events and add an overview in ContentStation to show the publish history.

#### History
 v1.0 Oktober 11, 2018, initial version

#### Disclaimer
This plugin and functionality is provided as-is and for free by GloveLike.

Questions, remarks or features can be send by email to [info@glovelike.nl](mailto:info@glovelike.nl) 

### Required skills
To install this plugin you will need basic admin and configuration knowledge of Enterprise/Aurora and be able to edit a PHP file. Also basic knowledge of the Digital Publish process is required.

### It is required to modify 'official' woodwing code!!
As there is currently no publish-event trigger in the publish process we will need to add this ourselves by making changes to the ContentStation server-plugin code as is provided by WoodWing. Although the change to the code is small it will be your responsibility to :

- understand the basic code idea behind the changes
- keep a copy of the original file
- test the functionality on a test system first.


## Installation

#### 1. Install the plugin
Copy the zipfile ``DigitalPublishHook.zip`` to the ``<enterprise>/config/plugins`` folder  and unzip. A folder with the name ``DigitalPublishHook`` should have been created. Make sure this folder has got read/write access for the webservice user.

#### 2. Add ContentStation functionality
Open the file ``<enterprise>/contentstation/config.js`` Look for the section ``plugins`` --> ``digitalEditor:``. 


Add the following line: 
``'../config/plugins/DigitalPublishHook/contentstation/DE_publishhistory.js',``

if this is the only line in that section or the last line, remove the comma on the end of the line.

Save the file and reload your contentStation in the browser, it might be required to clear the cache.

#### 3. Modify CsPubPublishArticleService.class.php

Make a copy of the file ``<enterprise>/config/plugins/ContentStation/services/pub/CsPubPublishArticleService.class.php`` just add ``.original`` to the copy

Open ``<enterprise>/config/plugins/ContentStation/services/pub/CsPubPublishArticleService.class.php`` and look for the function 

``public function runCallback( CsPubPublishArticleRequest $req )``

in that function, there is a line like:

``$properties = $req->Properties ? json_decode( $req->Properties ) : null;``

Just ABOVE that line, add the following code (can also be copied from this plugin folder/cs-plugin)

		// GloveLike-Mod start: 
		// Add hook to publish process so we can keep track of some things
		if ( file_exists(BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php') ){
			require_once (BASEDIR . '/config/plugins/DigitalPublishHook/DigitalPublishHook.php');
			if ( function_exists( 'DigitalPublishHook' ) ){
				DigitalPublishHook( $req );
			}
		}
		// GloveLike-Mod End:  
		
as an example there is a already patched ``CsPubPublishArticleService.class.php`` in that same 	``cs-plugin`` folder, so you can see how it should be. But please use the file from your own ContentStation plugin.	


## Testing
Now that everything is in place we can run a first test.

I assume that you have basic Digital Article publishing setup and working. If not please do so before continuing.

Also, do switch on debugging of the Enterprise server.

#### 1. Publish an article
Open an existing article or create a new one. Publish the article to one of your channels.

check: Did the article appear on your channel? if not, you might have made a mistake in the the patched file. Look for a php.log in the Enterprise logfolder, or in the subfolders.

#### 2. Check the database
Open your preferred tool to look into the database table ``smart_publishhistory``
A line should be added to this table with the objectId of the article you published

#### 3. Check the history in ContentStation
Close and reopen the article you just published. Click in the article (little bug in ContentStation).
In the menu-bar a new button should appear called ``Publish History[Last:y-m-d h:m:s]``
where the y-m-d etc is the last time this article was published

Now click the button. A pop-up should appear that will show which user published to what channel.


## Final configuration
You will notice that the external-id column shows a number like ``87e02227-a4d4-486e-95e0-025377535b74``. This is the ID of the publish channel in AWS

To be able to make this a recognizable string you can add an translation for this number in the ``DigitalPublishHook/config.php``

To 'know' the mapping between the number and the channel, you will need use one article and keep track of each channel you published to. When you then add the mapping to the config file the channels will be readable for all users
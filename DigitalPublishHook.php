<?php

/*
publish Req:CsPubPublishArticleRequest Object
(
[Ticket] => fd5ae254ItomGklkAqQSX4Hi0mfXDf5Ca3g7OdgW
[Action] => sns
[ArticleId] => 584
[PubChannelId] => 87e02227-a4d4-486e-95e0-025377535b74
[Properties] => {"brand":"1"}
[ArticleFiles] => Array
(
[0] => CsPubArticleFile Object
(
[Name] => output.html
[File] => Attachment Object
(
[Rendition] => output
[Type] => text/html
[Content] =>
[FilePath] =>
[FileUrl] => http://localhost/Enterprise10.5/transferindex.php?ticket=fd5ae254ItomGklkAqQSX4Hi0mfXDf5Ca3g7OdgW&fileguid=abff57ea-a4a1-47d2-af55-e116d25ddaea&format=text/html
[EditionId] =>
[ContentSourceFileLink] =>
[ContentSourceProxyLink] =>
[__classname__] => Attachment
)

[__classname__] => CsPubArticleFile
)

[1] => CsPubArticleFile Object
(
[Name] => output.psv
[File] => Attachment Object
(
[Rendition] => output
[Type] => application/xml
[Content] =>
[FilePath] =>
[FileUrl] => http://localhost/Enterprise10.5/transferindex.php?ticket=fd5ae254ItomGklkAqQSX4Hi0mfXDf5Ca3g7OdgW&fileguid=0deb4e28-f3d8-4bd6-bff2-08785f8ddb32&format=application/xml
[EditionId] =>
[ContentSourceFileLink] =>
[ContentSourceProxyLink] =>
[__classname__] => Attachment
)

[__classname__] => CsPubArticleFile
)

)

[__classname__] => CsPubPublishArticleRequest
)
*/

function DigitalPublishHook ( $req )
{
	LogHandler::Log( __CLASS__ . '-' . __FUNCTION__, 'DEBUG', 'publish Req:' . print_r($req,1) );
	
	// for first attempt we will insert in the smart_publishhistory table
	// so we need to collect some data
	$objectId = $req->ArticleId;
	$pubChannelId = $req->PubChannelId;
	
	// get the user from the ticket
	require_once( BASEDIR . '/server/bizclasses/BizSession.class.php' );
	$user = BizSession::checkTicket($req->Ticket );
	
	$publishedDate = date('Y-m-d\TH:i:s');
	
	$action = 'Publish';
	$majorVersion = '0';
	$minorVersion = '34';
	
	insertPublishHistory( $pubChannelId, $objectId,  $publishedDate, $action , $user, $majorVersion, $minorVersion);
	

}



function insertPublishHistory( $externalId, $objectId,  $publishedDate, $action , $user, $majorVersion, $minorVersion)
{
	$sql = 'insert into `smart_publishhistory` ';
	$sql .= '( `externalid`, `objectid` , `publisheddate` , `action`, `user`, `fieldsmajorversion`,`fieldsminorversion` )';
	$sql .= 'values ( ?,?,?,?,?,?,? )';
	$params = array($externalId, $objectId, $publishedDate, $action , $user, $majorVersion, $minorVersion);
	$dbh = DBDriverFactory::gen();
    $sth = $dbh->query( $sql, $params );
    
    return true;    
}


/*

 $sql = 'select * from `puborg_states` ';
        $sql .= ' where `id` = ? ';

        $params = array();
        $params[] = $newStatusId;

        $dbh = DBDriverFactory::gen();
        $sth = $dbh->query( $sql, $params );
        $row = $dbh->fetch( $sth );

        if ($row) {
            if ( $row['Export'] ){
                return true;
            }else{
                return false;
            }
        }
        return false;
*/

var gTicket ;
var gArticleId;
var gArticleName;


(function(){

	 // we need the ticket for calls to our service, so store in global
    var info = ContentStationSdk.getInfo();
	gTicket 		= info.Ticket;

	DigitalEditorSdk.onOpenArticle(function (article) {
	 	const enterpriseObject = article.getMetadata();
        console.log('wwEnterprise Object Metadata', enterpriseObject);
        gArticleId = enterpriseObject.MetaData.BasicMetaData.ID;
        gArticleName = enterpriseObject.MetaData.BasicMetaData.Name;
	 
		articleHasHistory();
	});

	 



	// Access to ContentStationSdk
    ContentStationSdk.showNotification({
        content: 'Publish History is loaded.'
    });
    
    
    
})();


function articleHasHistory(  )
{
	
	var submitObj = new Object();
	submitObj.command   = 'PublishHistory.articleHasHistory';
	submitObj.ticket    = gTicket;
	submitObj.params    = new Object();
    submitObj.params.articleId = gArticleId;
    submitObj.params.user = 'wvr';
    
	callAjax ( submitObj, function ( result) 
			  {
			  	
			  	if ( typeof  result.result.publisheddate !== 'undefined'){
			  		
					// load the button after the article is open, 
					DigitalEditorSdk.addToolbarButton({
						label: 'Publish History [Last:' + result.result.publisheddate + ']',
						onAction: function(button) {
							// hide button on click
							showPublishHistory();
						}
						}) 
			  	 }	
			  } 
			  );
}



function showPublishHistory()
{
	
	// send ping
	var submitObj = new Object();
	submitObj.command   = 'PublishHistory.getPublishHistory';
	submitObj.ticket    = gTicket;
	submitObj.params    = new Object();
    submitObj.params.articleId = gArticleId;
	callAjax ( submitObj, function ( result ) 
			  {
			  	 showHistoryData( result.result ); 	
			  } 
			  );
			  
			  
}


function showHistoryData( historyData )
{
	// create an html string from the data
	var html = '';
	html += '<table style="width: 100%;padding: 10px;" class="table table-striped">';
	// build header from first row
	var headerSeen = false;
	// show specific columns only
	var displayColumns = ['user','action','publisheddate','externalid'];
	
	// show all returned fields
	//var displayColumns = Object.keys(historyData.fieldinfo.fields);
	
	var primKey = historyData.fieldinfo.primaryKey;
	var primKeyValues = [];
	
	jQuery.each( historyData.rows ,
				function  ( index, row ) 
			 {
				var primKeyId;
				// render the header/column names
				if (!headerSeen)
				{
					html += '<tr style="height:25px;">';
					jQuery.each(displayColumns, 
						function ( index, fieldName ){
							if ( fieldName != primKey ){
								html += '<th style="text-align: left;">' + fieldName + '</th>';
							}	
						});
					html += '</tr>';
							 
					headerSeen = true;			 	
				};
				html += '<tr style="height:25px;">';
				// use order of displayColumns fields
				jQuery.each(displayColumns, 
					function ( index, fieldName ){
						
						if ( fieldName == primKey )
					   	{
							primKeyId = value;
							primKeyValues.push(primKeyId);
						}else{
							var dispValue = row[fieldName];
							if ( fieldName == 'publisheddate1' ){
								dispValue = dbDateToDisplayDateTime( dispValue );
							}
							html += '<td>' + dispValue + '</td>';
						}
					});
				
			 });	
	
	
	html += '</table>';

	
	
	var dialogId = ContentStationSdk.openModalDialog({
      title: 'Publish History for Article [' + gArticleName + ']',
      width: 1000,
      content: html,
      contentNoPadding: false,
      buttons: [
        // Button defined as secondary with class 'pale'
        // Has no callback defined - will close the dialog.
        {
          label: 'Close',
          class: 'pale'
        },
        
      ]
    });
}



function Ping()
{
	// send ping
	var submitObj = new Object();
	submitObj.command   = 'PublishHistory.ping';
	submitObj.ticket    = gTicket;
	callAjax ( submitObj, function ( result ) 
			  {
			  	 alert( result.result ); 	
			  } 
			  );
}



function getBaseURL () {
    return location.protocol + "//" + location.hostname + window.location.pathname.replace(/[\\\/][^\\\/]*$/, '');
}


function getEnterpriseURL()
{	
	ContentStationSdk = window.parent.ContentStationSdk;
	var info = ContentStationSdk.getInfo();
	var serverURL =  info.ServerInfo.URL.replace('index.php','');
	return serverURL;
}



function callAjax( request , callback)
{

	$.post( getEnterpriseURL() + "/config/plugins/DigitalPublishHook/index.php",  JSON.stringify( request ), 
		function( data ) {
        		var jsonResult = false;
        		try 
				  {
					jsonResult = JSON.parse( data );
					
				  }
				  catch (e)
				  {     
					console.log( "Error in parsing JSON [" + data  + ']' + JSON.stringify(request));
				  }
				  
				if ( jsonResult.hasOwnProperty('error') ) {
        			alert( jsonResult.error );
        			result = false;
        		} 
        		// show raised error in alert. can improve on this later
        		if ( jsonResult.hasOwnProperty('status') &&
        			 jsonResult.status == 'error' ) {
        			alert(   jsonResult.status + ':' + jsonResult.result );
        			result = false;
        		} 
        		 
				  
				if ( jsonResult !== false ) {
					callback ( jsonResult );
				}
				  
        	
 		 
		}
	);
}


function dbDateToDisplayDate( dbDate )
{
   var date = Date.parse( dbDate);
   return date.toString('dd-MM-yyyy');
}

function dbDateToDisplayDateTime( dbDate )
{
   //alert( dbDate );
   //var d = new Date();
   var d = Date.parse( dbDate);
   //return d.toString('dd-MM-yyyy HH:mm:ss');
   return d.toUTCString();
}

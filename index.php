<?php
// Load the enterprise config
require_once __DIR__ . '/../../config.php';
// plugin specific configuration
require_once __DIR__ . '/config.php';

// main entry point for this service
// if no post data is send, the normal (html) pages will be shown
// if POST data is available it will be send to the dispatch process
// dispatch process handles all post data request

include __DIR__ . '/server/dispatch.php';

// main check to see if any postdata is available
$inputSocket = fopen('php://input','rb');
$rawrequest = stream_get_contents($inputSocket);
fclose($inputSocket);

if ( $rawrequest != '' )
{
  // there seems to be some POST data, so lets send to dispatcher
  $request = json_decode( $rawrequest, true );
  if (method_exists ( 'LogHandler' , 'setLogName'))
  {
  	LogHandler::setLogName ( $request['command'] );
  }	
  print dispatch( $rawrequest ); // return the result of the call
}
else
{
   header("Location: startPage.php");
}
  
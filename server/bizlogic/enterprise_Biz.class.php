<?php

// bizLogic for Enterprise

require_once __DIR__ . '/../../config.php';
class enterpriseBiz 
{
	// ---------------------------
	// expose some dataClasses
	// ---------------------------
	public static function queryParam( $field, $operation, $value)
	{
		require_once BASEDIR. '/server/appservices/DataClasses.php';
		return new QueryParam ($field, $operation, $value);
	}



	// return: OK:  userObj
	// 		   fail: error	

	public static function validateTicket( $ticket )
	{
		// validation, check if the ticket is valid
		try {
			require_once( BASEDIR . '/server/bizclasses/BizSession.class.php' );
			LogHandler::Log( __CLASS__, 'DEBUG', 'Ticket is valid ' );
			$user = BizSession::checkTicket( $ticket );
			$userid = BizSession::getUserInfo( 'id' );
			return array('id' => $userid,
						 'user' => $user ,
						 'email' => BizSession::getUserInfo( 'email' ),
						 'fullname' => BizSession::getUserInfo( 'fullname' ),
						 );
		} catch ( BizException $e ) {
			// if we reach this point, we will not continue.
			LogHandler::Log( __CLASS__, 'DEBUG', 'ERROR:Not a valid ticket' );
			return  array(  'error' => 'Invalid ticket');
		}
	} 


	



	// perform a search on smart_objects
	// using the queryObjects
	// columns: comma seperated list of result columns
	// 
	public static function search( $ticket, $queryParams , $columns = false)
	{
		LogHandler::Log( __CLASS__, 'DEBUG', "search");
	
		require_once BASEDIR. '/server/interfaces/services/wfl/WflQueryObjectsRequest.class.php';
        require_once BASEDIR.'/server/services/wfl/WflQueryObjectsService.class.php';

		$resultColumns = array();
		if ( $columns ){
			$resultColumns = explode(',',$columns);
		}
		
        $request = new WflQueryObjectsRequest(
            $ticket,  // ticket
            $queryParams,   // params
            null,           // firstEntry
            0,              // MaxEntries
            false,           // Hierarchical
            null,           // Order
            null,           // Minimal props
            $resultColumns,           // Requested props
            null,           // Areas
            null            // GetObjectMode
        );

        $service = new WflQueryObjectsService();
        $response = $service->execute($request);
		$result = self::queryResultToArray( $response );
		return $result;
	}


	// get all the ids from the objects in the queryResult
	// column to look for is 'ID'
	// if filterType is specified only do this for that type 
	public static function getObjectIdsFromQueryResult( $queryResult, $filterType = false)
	{
		//LogHandler::Log( __CLASS__, 'DEBUG', "queryResult:" . print_r( $queryResult,1));
		// try to find our ID column
		$idCol = -1; 
		$typeCol = -1;
		foreach ( $queryResult['Columns'] as  $index => $col )
		{
			if ( $col->Name == 'ID' )
			{
				$idCol = $index;
			}
			if ( $col->Name == 'Type' )
			{
				$typeCol = $index;
			}
		}		
		
		if ( $idCol == -1 )
		{
			return false; // no ID column
		}
		$ids = array();
		foreach ( $queryResult['Rows'] as  $index => $Row	 )
		{
			$ids[] = $Row[$idCol];
		}
		
		return $ids;
	}

	
	// -getPreview-
	// get the preview or thumb to our tempfolder
	// return the filelinks via fileloader
	
	public static function getObjPreview( $ticket, $ids, $rendition )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "getObjPreview");
	
		require_once BASEDIR. '/server/interfaces/services/wfl/WflGetObjectsRequest.class.php';
        require_once BASEDIR.'/server/services/wfl/WflGetObjectsService.class.php';

        $req = new WflGetObjectsRequest( 
        	$ticket, 
        	$ids, 
        	$lock= false,
        	$rendition, 
        	array('NoMetaData') 
        	);
        $service = new WflGetObjectsService();
        $response = $service->execute( $req );

		LogHandler::Log( __CLASS__, 'DEBUG', "response:" . print_r($response,1));
      
        // download
        require_once BASEDIR . '/server/bizclasses/BizTransferServer.class.php';
		$transferServer = new BizTransferServer();
		$previewInfo = array();
		foreach ( $response->Objects as $object )
		{
			$objID = $object->MetaData->BasicMetaData->ID;
			
			foreach ( $object->Files as $attachment ) { 
				$exportName = 'puog-' . $ticket . '-' . $objID . '-' . $attachment->Rendition . '.jpg';
				$exportFile = PUOG_BASE_TEMPFOLDER . '/' . $exportName;
				LogHandler::Log( __CLASS__ . ':' . __FUNCTION__, 'DEBUG', 'ExportFile:' . $exportFile );
				if ( $transferServer->copyFromFileTransferServer( $exportFile, $attachment ) ) {
					// if image was retrieved correct, add to structure
					$image = $exportName;
					$transferServer->deleteFile( $attachment->FilePath );
					$previewInfo[ $objID] = array();
					$file = str_replace( PUOG_PDF_TEMPFOLDER ,'', $exportFile);
					$previewInfo[ $objID][$attachment->Rendition] = PUOG_MY_PATH ."/FileLoader.php?file=$file";
				}
			}		
		}			
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "response:" . print_r($previewInfo,1));
        return $previewInfo;
	}
	
	// -getNative-
	// get the native rendition to our tempfolder
	// the tempfolder variable can have a placeholder {id} that can be subsituted
	// return the filelinks via fileloader
	
	public static function getObjNativeToFile( $ticket, $ids, $tempFile )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "getObjNative");
	
		require_once BASEDIR. '/server/interfaces/services/wfl/WflGetObjectsRequest.class.php';
        require_once BASEDIR.'/server/services/wfl/WflGetObjectsService.class.php';

        $req = new WflGetObjectsRequest( 
        	$ticket, 
        	$ids, 
        	$lock= false,
        	'native', 
        	array('NoMetaData') 
        	);
        $service = new WflGetObjectsService();
        $response = $service->execute( $req );

		//LogHandler::Log( __CLASS__, 'DEBUG', "response:" . print_r($response,1));
      
        // download
        require_once BASEDIR . '/server/bizclasses/BizTransferServer.class.php';
		$transferServer = new BizTransferServer();
		$fileInfo = array();
		foreach ( $response->Objects as $object )
		{
			$objID = $object->MetaData->BasicMetaData->ID;
			
			foreach ( $object->Files as $attachment ) { 
				$tempFile = str_replace('{id}', $objID , $tempFile );
				//LogHandler::Log( __CLASS__ . ':' . __FUNCTION__, 'DEBUG', 'tempFile:' . $tempFile );
				if ( $transferServer->copyFromFileTransferServer( $tempFile, $attachment ) ) {
					$transferServer->deleteFile( $attachment->FilePath );
					$fileInfo[ $objID] = array();
					$fileInfo[ $objID]['native'] = $tempFile;
				}
			}		
		}			
		//LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "response:" . print_r($fileInfo,1));
        return $fileInfo;
	}



	// create a complete 'Path' in the enterprise server
	// parameters are strings/names not ID's
	// for now:
	// - we expect brand does exist
	// - we expect channel does exist
	public static function createPublicationStructure ( $ticket, $brand = null , $channel = null, $issue = null, $category = null, $dossier = null )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "createPublicationStructure  $brand  , $channel , $issue , $category, $dossier");
		// check brand
		$brandId = self::brandExists( $brand );
		if (! $brandId ){
			LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "$brand does not exist, please create");
			return false;
		}
		
		// check channel
		$channelId = self::channelExists( $brand , $channel );
		if (! $channelId ){
			LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "$brand / $channel does not exist, creating now");
			
			// see if we can create this channel
			$newPubChannel = self::newPubChannel();
			$newPubChannel->Name = $channel;
			$newPubChannel->PublicationId = $brandId;
			$channelId = self::createPubChannel( $ticket, $brandId, $newPubChannel);
			if (! $channelId ) {
				LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "unable to create channel");
				return false;
			}	
			return false;
		}
		
		// check issue, if not found, create it
		// check channel
        $issueId = self::issueExists( $channelId, $issue );
		if (! $issueId ){
			LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "$brand / $channel / $issue does not exist,creating now");
			// create issue
			$newIssue = self::newIssue();
			$newIssue->Name = $issue;
			$newIssue->Description = 'Added by PubOrganiser';
            $newIssue->Activated = true;
			$issueId = self::createIssue( $ticket, $brandId, $channelId, $newIssue);
			if (! $issueId ) {
				LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "unable to create issue");
				return false;
			}	
		}

		$categoryId = self::categoryExists( $brandId , $category);
		if (! $categoryId ){
			LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "$brand / $category  does not exist,creating now");
			// create issue
			$newCat = self::newCategory();
			$newCat->Name = $category;
			$issueId = self::createCategory( $ticket, $brandId, $newCat);
			if (! $issueId ) {
				LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "unable to create Category");
				return false;
			}	
		}

		// create the dossier
        $dossierId = self::dossierExists( $brandId , $issueId, $dossier);
        if (! $dossierId ){
            LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Dossier [$dossier]  does not exist,creating now");
            // create issue
            $statusInfo = self::getFirstObjStatus( $brandId, 'Dossier' );
            $newDossier = self::newObject();
            $newDossier->MetaData->BasicMetaData->Name = $dossier;
            $newDossier->MetaData->BasicMetaData->Type = 'Dossier';
            $newDossier->MetaData->BasicMetaData->Publication->Id =   $brandId;
            $newDossier->MetaData->BasicMetaData->Category->Id = $categoryId;
            $newDossier->MetaData->WorkflowMetaData->State->Id = $statusInfo['id'];
            $newDossier->MetaData->WorkflowMetaData->State->Name = $statusInfo['state'];

            $target = new Target($channelId,$issueId);
            $target->PubChannel = new PubChannel($channelId);
            $target->Issue      = new Issue($issueId);
            $newDossier->Targets = array($target);
            LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG','newDossier:' . print_r( $newDossier,1));

            $dossierId = self::createObject( $ticket, $newDossier);
            if (! $dossierId ) {
                LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "unable to create Dossier");
                return false;
            }

        }
		
		return $dossierId; // we need that one later
	}
	
	
	

/*
	public static function getIssuesAdm( $ticket, $brandId, $channelId )
	{
		require_once BASEDIR.'/server/interfaces/services/adm/AdmGetIssuesRequest.class.php';
        require_once BASEDIR.'/server/services/adm/AdmGetIssuesService.class.php';
		// now get the issue
        $service = new AdmGetIssuesService();
        $request = new AdmGetIssuesRequest($ticket, array(), (int)$brandId, (int)$channelId, array(1));
        $response = $service->execute($request);
		
		LogHandler::Log( __CLASS__, 'DEBUG', "Issues:" . print_r( $response,1));
		
		
		return $response->Issues;
	
	}
	*/
	
	public static function brandExists( $brandName )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Checking brand:$brandName");
		$dbh = DBDriverFactory::gen();
		$sql = " select id from `smart_publications` where publication=? ";
		$params = array($brandName);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		if ( $row !== false ){
			return $row['id'];
		}	
		
		return false;
	}
	
	
	private static function channelExists( $brandName , $channelName)
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Checking channel:$channelName");
		$dbh = DBDriverFactory::gen();
		$sql = "select id  from `smart_channels` where `publicationid` in ";
		$sql .= " (select id from `smart_publications` where publication=?) ";
		$sql .= " and name = ? ";
		$params = array($brandName, $channelName);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		if ( $row !== false ){
			return $row['id'];
		}	
		return false;
	}
	
	private static function issueExists(  $channelId, $issueName)
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Checking issue:$issueName");
		$dbh = DBDriverFactory::gen();
		$sql = "select id  from `smart_issues` where `channelid` = ? ";
		$sql .= " and name = ? ";
		$params = array($channelId, $issueName);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		if ( $row !== false ){
			return $row['id'];
		}	
		return false;
	}
	
	private static function categoryExists(  $brandId, $categoryName)
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Checking category:$categoryName");
		$dbh = DBDriverFactory::gen();
		$sql = "select id  from `smart_publsections` where `publication` = ? ";
		$sql .= " and section = ? ";
		$params = array($brandId, $categoryName);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		if ( $row !== false ){
			return $row['id'];
		}	
		return false;
	}

    private static function dossierExists(  $brandId, $issueId, $dossierName )
    {

        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "brand:$brandId, issue:$issueId Checking for dossier:$dossierName");
        $dbh = DBDriverFactory::gen();
        $sql = "select id  from `smart_objects` where `name` = ? ";
        $sql .= " and publication = ? ";
        $sql .= " and type='Dossier'";
        $params = array( $dossierName, $brandId);
        $sth = $dbh->query( $sql, $params );
        if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
        $status = array();
        $row = $dbh->fetch( $sth );
        if ( $row !== false ){
            return $row['id'];
        }
        return false;
    }

    // $dossierId = dossier to add objects to
    // $objects = array of objectId
    public static function addToDossier( $ticket, $dossierId, $objects)
    {
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "Adding to dossier:$dossierId ids:" . print_r($objects,1));
        require_once BASEDIR . '/server/interfaces/services/wfl/WflCreateObjectRelationsRequest.class.php';
        require_once BASEDIR . '/server/services/wfl/WflCreateObjectRelationsService.class.php';


        $relations = array();
        foreach ( $objects as $obj)
        {
            $relation = new Relation();
            $relation->Parent = $dossierId;
            $relation->Child = $obj['Id'];
            $relation->Type = 'Contained';
            $relations[] = $relation ;
        }


        try {
            $service = new WflCreateObjectRelationsService();
            $req = new WflCreateObjectRelationsRequest( $ticket, $relations );
            $response = $service->execute( $req );
        } catch ( Exception $e ) {
            $message = $e->getMessage();
            LogHandler::Log( __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', 'Unable to set relations between task and dossier' );
            LogHandler::Log( __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', $message );
            return false;
        }

        return true;
    }



	
	public static function newPubChannel()
	{
		require_once BASEDIR . "/server/interfaces/services/adm/DataClasses.php";
		return new AdmPubChannel();
	}
	
	public static function createPubChannel( $ticket, $brandId, $channelObj ) 
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', sprintf("Enter: %s", __METHOD__));
        require_once BASEDIR.'/server/interfaces/services/adm/AdmCreatePubChannelsRequest.class.php';
        require_once BASEDIR.'/server/services/adm/AdmCreatePubChannelsService.class.php';
        
		$service = new AdmCreatePubChannelsService();
        $request = new AdmCreatePubChannelsRequest($ticket, array(), $brandId,  array($channelObj));
        $response = $service->execute($request);
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', print_r($response,1));
		if (isset($response->Issues[0])){
            $issue = $response->Issues[0];
            return (int) $issue->Id;
        }
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG','Unable to create Issue, Return: false' );
        return false;
	}
	
	
	
	public static function newIssue()
	{
		require_once BASEDIR . "/server/interfaces/services/adm/DataClasses.php";
		return new AdmIssue();
	}
	
	public static function createIssue( $ticket, $brandId, $channelId, $issueObj) {
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', sprintf("Enter: %s", __METHOD__));
        require_once BASEDIR.'/server/interfaces/services/adm/AdmCreateIssuesRequest.class.php';
        require_once BASEDIR.'/server/services/adm/AdmCreateIssuesService.class.php';

        $service = new AdmCreateIssuesService();
        $request = new AdmCreateIssuesRequest($ticket, array(), $brandId, $channelId, array($issueObj));
        $response = $service->execute($request);

        if (isset($response->Issues[0])){
            $issue = $response->Issues[0];
            return (int) $issue->Id;
        }
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG','Unable to create Issue, Return: false' );
        return false;
    }

    public static function getIssues( $ticket, $brandId, $channelId )
    {
        $IssueMdFields = unserialize ( PUOG_ISSUEMDFIELDS );
        $dbh = DBDriverFactory::gen();
        $sql = "select id,name,publdate from `smart_issues` where `channelid` = ?";
        $sql .= ' order by publdate,name';
        $params = array($channelId);
        $sth = $dbh->query( $sql, $params );
        if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
        $issues = array();
        while ($row = $dbh->fetch( $sth ))
        {
            $issues[] = $row;
        }

        LogHandler::Log( __CLASS__, 'DEBUG', "Issues:" . print_r( $issues,1));
        // get the custom metadata for each issue
        $returnIssues = array();
        foreach ( $issues as $issue )
        {
            $issId = (int)$issue['id'];
            $sql = "select name,value from `smart_channeldata` where issue = ? and name like 'C_PUOG%'";
            $params = array($issId);
            $sth = $dbh->query( $sql, $params );
            while ($row = $dbh->fetch( $sth ))
            {
                $colName = str_replace('C_PUOG_','',$row['name']);
                $mdName = $IssueMdFields[$colName]['label'];
                $issue[ $mdName ] = $row['value'];
            }
            LogHandler::Log( __CLASS__, 'DEBUG', "Issue:" . print_r( $issue,1));
            $returnIssues[] = $issue;
        }
        return $returnIssues;

    }


    public static function newCategory()
	{
        require_once BASEDIR . "/server/interfaces/services/adm/DataClasses.php";
        return new AdmSection();
	}
	
	
	public static function createCategory($ticket, $brandId,  $categoryObj) {
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', sprintf("Enter: %s", __METHOD__));

        require_once BASEDIR.'/server/interfaces/services/adm/AdmCreateSectionsRequest.class.php';
        require_once BASEDIR.'/server/services/adm/AdmCreateSectionsService.class.php';

        $service = new AdmCreateSectionsService();
        $request = new AdmCreateSectionsRequest($ticket, array(), $brandId, null, array($categoryObj));

        $response = $service->execute($request);

        if (isset($response->Sections[0])){
            $category = $response->Sections[0];
            return (int) $category->Id;
        }
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG','Unable to create Category, Return: false' );
        return false;
    }


    public static function newObject()
    {
        require_once BASEDIR . "/server/interfaces/services/wfl/DataClasses.php";
        $object = new Object();
        $object->MetaData = new MetaData();
        $object->MetaData->BasicMetaData = new BasicMetaData();
        $object->MetaData->BasicMetaData->Publication = new Publication();
        $object->MetaData->BasicMetaData->Category = new Category();
        $object->MetaData->WorkflowMetaData = new WorkflowMetaData();
        $object->MetaData->WorkflowMetaData->State = new State();
        return $object;
    }

    public static function createObject($ticket, $object ) {
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', sprintf("Enter: %s", __METHOD__));

        // WVR
        require_once BASEDIR.'/server/services/wfl/WflCreateObjectsService.class.php';
        require_once BASEDIR.'/server/interfaces/services/wfl/WflCreateObjectsRequest.class.php';


        $request = new WflCreateObjectsRequest($ticket, false, array($object), null, null);
        $service = new WflCreateObjectsService();
        $response = $service->execute($request);

        if (isset($response->Objects[0]->MetaData->BasicMetaData->ID )){

            return (int) $response->Objects[0]->MetaData->BasicMetaData->ID;
        }
        LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG','Unable to create Object, Return: false' );
        return false;
    }

	// ----------------------------
	// status related functions
	// ----------------------------

	public static function getFirstObjStatus( $brand, $objType )
	{
		$dbh = DBDriverFactory::gen();
		$sql = "select id,state from `smart_states` where `publication` =? ";
		$sql .= " and `type` = ? ";
		$sql .= " order by code asc";
		$params = array($brand, $objType);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		$status = $row;
		LogHandler::Log( __CLASS__, 'DEBUG', "status:" . print_r($status,1));
		return $status;
	}



	private static function queryResultToArray( $queryResponse )
	{
		$columns = $queryResponse->Columns;
		$rows 	 = $queryResponse->Rows;
		
		$data = array();
		foreach ($rows as $row )
		{
			$datarow = array();
			foreach ($row as $index => $value )
			{
				$colName = $columns[$index]->Name;
				$datarow[$colName] = $value;
			}
			$data[] = $datarow;
		}
		
		$result = array();
		$result['TotalEntries'] =  $queryResponse->TotalEntries;
		$result['Data'] = $data;
		$result['Rows'] = $rows;
		$result['Columns'] = $columns;
		return $result;
	}

	
	
	public static function getSmartObjectField( $objId, $field )
	{
		$dbh = DBDriverFactory::gen();
		$sql = "select `$field` from `smart_objects` where `id` = ? ";
		$params = array($objId);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$row = $dbh->fetch( $sth );
		if ( $row ) { return $row[$field]; }
		LogHandler::Log( __CLASS__, 'DEBUG', "status:" . $status);
		return false;
	}



}
<?php

// bizLogic for Enterprise


class authorizationBiz 
{
	
	// if you do not want to validate on ticket, simply return a true
	public static function isValidTicket( $ticket )
	{
		// we validate the ticket against Enterprise
		require_once __DIR__ . '/enterprise_Biz.class.php';
		$sourceUser = enterpriseBiz::validateTicket($ticket );
		if ( array_key_exists('error' , $sourceUser )) { return false; }
		return true;
	}
	
	
	
	public static function isValidUser( $user )
	{
		// no validation (yet) on user
		return true;	
	
	}

	


}
<?php

// bizLogic for Enterprise

require_once __DIR__ . '/../../config.php';

class PublishHistoryBiz 
{
	
	
	public static function articleHasHistory( $articleId )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "articleHasHistory $articleId");
		$dbh = DBDriverFactory::gen();
		$sql = " select * from `smart_publishhistory` where `objectid`=? order by `publisheddate` desc";
		$params = array($articleId);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		$row = $dbh->fetch( $sth );
		if ( $row !== false ){
			return $row;
		}	
		
		return false;
	}
	
	
	public static function getPublishHistory( $articleId )
	{
		LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "articleHasHistory $articleId");
		$channelMap = unserialize(DPH_CHANNELID_MAP);
		$history = array();
		$history['rows'] = array();
		$history['fieldinfo'] = self::getTableColumns( 'smart_publishhistory' );
		
		$dbh = DBDriverFactory::gen();
		$sql = " select * from `smart_publishhistory` where `objectid`=? order by `publisheddate` desc";
		$params = array($articleId);
		$sth = $dbh->query( $sql, $params );
		if ( $sth === false || $sth == '' ) { LogHandler::Log( __CLASS__, 'DEBUG',"Error executing SQL [$sql]");return false;}
		$status = array();
		while ( $row = $dbh->fetch( $sth )){
			// replace info
			if ( array_key_exists( $row['externalid'], $channelMap )){
				$row['externalid'] = $channelMap[$row['externalid']]['Channel'] . '-' . $channelMap[$row['externalid']]['Name'];
			}
			$history['rows'][] = $row;
		}	
		
		return $history;
	}
	
	
	
	// return the table structure
	private static function getTableColumns( $tableName )
	{
		$sql = "SHOW COLUMNS FROM $tableName " ;
		$params = array();
		$dbh = DBDriverFactory::gen();
		$sth = $dbh->query( $sql, $params );
		$fields = array();
		// see if we can find an primary key
		$primaryKey = '';
		while ($row = $dbh->fetch( $sth ))
		{
			if ( $row['Key'] == 'PRI' && $primaryKey=='') { $primaryKey = $row['Field'];}
			$type = explode('(',$row['Type']);
			
			$fields[$row['Field']] = $type[0];
		}
		
		$fieldInfo = array();
		$fieldInfo['primaryKey'] = $primaryKey;
		$fieldInfo['fields'] 	 = $fields;
		
		//LogHandler::Log(  __CLASS__ . ':' . __FUNCTION__ , 'DEBUG', "fields:" . print_r($fieldInfo,1));
		return $fieldInfo;
	}
	
	


}
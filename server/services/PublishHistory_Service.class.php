<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../bizlogic/PublishHistory_Biz.class.php';

class PublishHistoryService 
{

	// basic function to quickly test the class
	public function ping()
	{
		$serverVersion = 'v0.1';
		return  json_encode( array( 'status' => 'OK',
									'result' => __CLASS__ . " ($serverVersion), timestamp:" . date('Y-M-d H:i:s'),
									 	));
	}
	
	
	public function articleHasHistory( $request )
	{
		
		$hasHistory = PublishHistoryBiz::articleHasHistory( $request['articleId'] );
	
		return  json_encode( array( 'status' => 'OK',
									'result' => $hasHistory,
									 	));
	
	}
	
	
	
	public function getPublishHistory( $request )
	{
		$History = PublishHistoryBiz::getPublishHistory( $request['articleId'] );
		return  json_encode( array( 'status' => 'OK',
									'result' => $History,
									 	));
	}


}
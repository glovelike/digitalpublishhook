<?php




// set a global variable that will hold the
// current command being executed
$currentCommand = '';
$logTimeStamp = '';

function dispatch( $rawRequest)
{
  global  $currentCommand, $logTimeStamp;
  
  LogHandler::Log( '-dispatcher-', 'DEBUG','============================');
  LogHandler::Log( '-dispatcher-', 'DEBUG','raw_data [' . $rawRequest . ']');

  // convert to object
  $request = json_decode( $rawRequest, true );
  
  
  
  $success = false;
  $result  = '';
  
  if ( $request != '' )
  {
  
    // run some basic checks, we need at least datatype, domainId, contestDate
    if ( ! array_key_exists('command',    $request ) ) { $result = $result . 'Error: command-key not specified in request'    ;}
    else
    {
      // it might be that this is a simple 'ping' so return the datetime
      if ( $request['command'] == 'ping' ) 
      { $success = true;  
        $result = json_encode(array('timestamp' => date("Y-m-d H:i:s")));
      }
      else
      { 
        // if needed you can run checks on this point
        $result = '';
      }  
        
    }  
  }
  
  
  // handle the command
  $command = $request['command'];
  // for logging
  $currentCommand = $command;
  //$logTimeStamp   = getLogTimeStamp();
  
  LogHandler::Log( '-dispatcher-', 'DEBUG','request [' . print_r($request,1) . ']');
  // write command to logfolder
  //MyLogRequest ( $currentCommand, $request );
  LogHandler::Log( '-dispatcher-', 'DEBUG','------------------------------');
  LogHandler::Log( '-dispatcher-', 'DEBUG',"Command [$command]");
  
  // get class of command
  $classFunction = explode('.',  $command ); 
  $class    = $classFunction[0];
  $function = $classFunction[1];
  LogHandler::Log( '-dispatcher-', 'DEBUG',"Class    [$class]");
  LogHandler::Log( '-dispatcher-', 'DEBUG',"Function [$function]");
  
  // see if we can find a file that should handle the class
  $classdir = dirname(__FILE__) . '/services';
  $classfile = $classdir . '/' . $class . '_Service.class.php';
  LogHandler::Log( '-dispatcher-', 'DEBUG',"classfile [$classfile]");
  
  if ( ! file_exists( $classfile ) )
  {
    LogHandler::Log( '-dispatcher-', 'DEBUG',"classfile not found!");
     $result = json_encode( array('error' => 'Invalid class specified [' . $command . ']') );
  }
  else
  {
     // load the class
     include_once $classfile;
     $class = $class . 'Service';
     if( !class_exists( $class ) )
     {
       LogHandler::Log( '-dispatcher-', 'DEBUG',"ClassFile does not contain class");
       $result = json_encode( array('error' => 'ClassFile does not contain class [' . $command . ']') );
     }
     
    
     if ( $result == '')
     {
       // create the class
       $myClass = new $class; 
       if ( ! method_exists( $myClass, $function ))
       {
         LogHandler::Log( '-dispatcher-', 'DEBUG',"Invalid function!");
         $result = json_encode( array('error' => 'Invalid function specified [' . $command . ']') );
       }
     }
     // all should be in place to do the thing
     if ( $result == '')
     {
        $success = true;
        
        // handle possible ticket only once, by calling the autorization_Biz::isValidTicket
        // you need to implement this, if you do not want to use Enterprise
        require_once __DIR__ . '/bizlogic/authorization_Biz.class.php';
        if ( ! authorizationBiz::isValidTicket( $request['ticket'] ) )
        {
        	$result = json_encode( array('error' => 'Not a valid user') );
        }else{
        	$request['params']['ticket'] = $request['ticket']; // add ticket so we can use it later
	        $result  = $myClass->$function( $request['params'] );  // must return a JSON object
        }
     	LogHandler::Log( '-dispatcher-', 'DEBUG',"result of function call");
     	LogHandler::Log( '-dispatcher-', 'DEBUG', $result );
     }
  
  }
  
  
 LogHandler::Log( '-dispatcher-', 'DEBUG',"Success:$success");
  // LogHandler::Log( '-dispatcher-', 'DEBUG',"Result [$result]");
 LogHandler::Log( '-dispatcher-', 'DEBUG','+--------------------------');
 // MyLogResponse ( $currentCommand, $result ); 
  
  
  if ($success) {
	header('HTTP/1.1 200 OK');
    echo $result;
  }
  else {
	//header('HTTP/1.1 403 Internal Server Error');
	header('HTTP/1.1 200 OK');  // we do as if OK, but result can contain status and message
	echo $result;
  }  
} 

